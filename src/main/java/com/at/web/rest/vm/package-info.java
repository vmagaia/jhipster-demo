/**
 * View Models used by Spring MVC REST controllers.
 */
package com.at.web.rest.vm;
